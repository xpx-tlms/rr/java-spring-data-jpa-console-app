package com.example.demo;

import com.example.demo.model.Cat;
import com.example.demo.model.Person;
import com.example.demo.model.Product;
import com.example.demo.repository.CatRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.repository.ProductRepository;

import java.util.Arrays;

public class  DBPopulator {

    public static void populate(CatRepository catRepository, ProductRepository productRepository, PersonRepository personRepository) {

        // Cats
        if (catRepository.findAll().size() == 0) {
            System.out.println("*** Add data to CAT table ***");
            var catList = Arrays.asList(
                new Cat("Gypsy", 4, "black"),
                new Cat("Rocky", 4, "white"),
                new Cat("Baxie", 5, "black & white"),
                new Cat("Gabby", 6, "brown"),
                new Cat("Lily", 7, "black & brown")
            );
            catList.forEach(catRepository::save);
        }

        // Products
        if (productRepository.findAll().size() == 0) {
            System.out.println("*** Add data to PRODUCT table ***");
            var productList = Arrays.asList(
                    new Product(1, "12 inch speaker", 10.00F, "spk-12"),
                    new Product(1, "10 inch speaker", 9.00F, "spk-10"),
                    new Product(1, "10 inch speaker", 8.00F, "spk-08"),
                    new Product(2, "Bannana", 1.11F, "food-01"),
                    new Product(2, "Apple", 2.22F, "food-02"),
                    new Product(2, "Orange", 3.33F, "food-03")
            );
            productList.forEach(productRepository::save);
        }

        // People
        if (personRepository.findAll().size() == 0) {
            System.out.println("*** Add data to PERSON table ***");
            var personList = Arrays.asList(
                    new Person("Alice", "Deer", "111-11-1111", 11),
                    new Person("Bob", "Smith", "222-22-2222", 22),
                    new Person("Charlie", "Jones", "333-33-3333", 33),
                    new Person("Dan", "Updegraff", "444-44-4444", 44),
                    new Person("Egar", "Harly", "555-55-55555", 55),
                    new Person("Frank", "Gamble", "666-66-6666", 66),
                    new Person("Guthrie", "Govan", "777-77-7777", 77),
                    new Person("Harry", "Code", "888-88-8888", 88)
            );
            personList.forEach(personRepository::save);
        }
    }
}
